#pragma once

#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/cURLpp.hpp>
#include <fstream>
#include <string>

class FileSender
{

public:
    int postToServer(const std::string& url, const std::string& fileName);
};