#include "DataGenerator.hpp"
#include "FileSender.hpp"

int main()
{
    // Did this so ptsv2 dump store won't ban.
    for (int i = 0; i <= 10; ++i)
    {
        DataGenerator dataGenerator{};
        std::string fileName = "data" + std::to_string(i) + ".txt";
        dataGenerator.writeToFile(fileName);
        FileSender fileSender{};
        // dump service  http://ptsv2.com/
        int status = fileSender.postToServer("http://ptsv2.com/t/7li6d-1593620028/post", fileName);
        if (status == 0)
        {
            std::remove(fileName.c_str());
        }
        else
        {
            break;
            // should be some retry mechanism
        }
    }
    return 0;
}