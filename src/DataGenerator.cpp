
#include "DataGenerator.hpp"

using namespace std::chrono;

void DataGenerator::writeToFile(const std::string& fileName)
{

    int64_t timestamp = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();

    std::this_thread::sleep_for(std::chrono::milliseconds(rand() % (1501 - 500) + 500));
    std::ofstream dataFile;
    dataFile.open(fileName);
    dataFile << rand() % 100 << " " << timestamp;
    dataFile.close();
}
