#include "FileSender.hpp"

int FileSender::postToServer(const std::string& url, const std::string& fileName)
{
    curlpp::Cleanup cleaner;
    std::ifstream input(fileName);
    std::string line;
    std::getline(input, line);

    try
    {
        curlpp::Cleanup cleaner;
        curlpp::Easy request;

        request.setOpt(new curlpp::options::Url(url));
        request.setOpt(new curlpp::options::Verbose(false));
        request.setOpt(new curlpp::options::FailOnError(true));

        std::list<std::string> header;
        header.push_back("Content-Type: application/octet-stream");
        request.setOpt(new curlpp::options::HttpHeader(header));

        request.setOpt(new curlpp::options::PostFields(line));
        request.setOpt(new curlpp::options::PostFieldSize(line.length()));

        request.perform();
    }
    catch (curlpp::LogicError& e)
    {
        std::cout << e.what() << std::endl;
        return -1;
    }
    catch (curlpp::RuntimeError& e)
    {
        std::cout << e.what() << std::endl;
        return -1;
    }

    return EXIT_SUCCESS;
}